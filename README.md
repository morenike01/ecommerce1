# ecommerce
An updated version of the existing files
tphp.php, json.php, httpsj.php = these are javascript validation files

phpval.php = this is the php validation file

nikecode.php = this contains the prepared statement and xss (in the search query)

myerror.txt = this contains the error file

httpauth = this is the http authentication file

error_list.php, cerror.php, badpage.php, errorlogging .php = custom error handling and the 404 error handling files
